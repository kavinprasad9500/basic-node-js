const dotenv = require("dotenv").config();
const nodemailer = require("nodemailer");
const Service = require("./models/ServiceModel");

// Function to send an email
const sendEmail = async (userDetail,Subject, BookingDetails) => {
  // Create a transporter object
  // console.log(process.env.EMAIL_USER);
  const transporter = await nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASS,
    },
  });

  const name = await userDetail.username;
  // const email = await userDetail.email;
  // const mobileNo = await userDetail.mobileNumber;
  
  const serviceNames = [];
  const services= await BookingDetails.service;
  // Iterate through the service IDs and fetch the service names
  for (const serviceId of services) {
    const service = await Service.findById(serviceId);
    if (service) {
      // Push the service name to the array if it exists
      serviceNames.push(service.name); // Assuming your service model has a 'name' field
    }
  }
  
  const bikeModel = await BookingDetails.bikeModel;
  const serviceDate = await BookingDetails.bookingDate;
  const status = await BookingDetails.status;

  // Email details
  const htmlContent = `
  <!DOCTYPE html>
  <html>
  
  <head>
      <title>Bike Service Reminder</title>
  </head>
  
  <body>
      <h1>Welcome to Our Bike Service Company</h1>
      <p>Dear ${name},</p>
      <p>We hope this message finds you well. It's time to schedule your bike service with us!</p>
  
      <h2>Service Details:</h2>
      <ul>
          <li>Service Model: ${bikeModel}</li>
          <li>Service Date: ${serviceDate}</li>
          <li>Services: ${serviceNames}</li>
          <li>Status: ${status}</li>
      </ul>
  
      <p>Your Bike is due for service on ${serviceDate}. Our team will be ready to assist you.</p>
  
      <p>If you have any questions or need to reschedule your service appointment, please contact us at ${process.env.EMAIL_USER}.</p>
  
      <p>Thank you for choosing our bike service company. We look forward to serving you!</p>
  
      <p>Sincerely,</p>
      <p>The Bike Service Team</p>
  </body>
  
  </html>
`;
  const mailOptions = {
    from: process.env.FROM_EMAIL,
    to: userDetail.email,
    subject: Subject,
    // text: `${BookingDetails}`,
    html: htmlContent,
  };

  // Send the email
  await transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.error("Error sending email:", error);
    } else {
      console.log("Email sent successfully:", info.response);
    }
  });
};

module.exports = sendEmail;