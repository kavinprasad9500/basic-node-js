const whitelist = [
  "https://kavinprasad.me",
  "http://127.0.0.1:4444",
  "https://localhost:4444",
  "https://www.google.com",
];
corsOptions = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1 || !origin) callback(null, true);
    else callback(new Error("Not allowed by CORS"));
  },
  optionsSuccessStatus: 200,
};

module.exports = corsOptions;