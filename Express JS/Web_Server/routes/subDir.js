const express = require('express')
const router = express.Router();

const path = require('path');

router.get("^/$|/index(.html)?", (req, res) => {
    res.sendFile(path.join(__dirname, "..", "app", "sub", "index.html"));
    // res.send('Hello World!');
  });

module.exports = router;