const express = require("express");
const router = express.Router();
const path = require("path");
const employeController = require("../../Controllers/employeeController");

router.route("/").
    get(employeController.getAllEmployees)
    .post(employeController.createNewEmployee)
    .put(employeController.updateEmployee)
    .delete(employeController.deleteEmployee);

router.route("/:id").get(employeController.getEmployee);

module.exports = router;
