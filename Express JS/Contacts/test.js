// const { MongoClient } = require("mongodb");

// const url = "mongodb://kavin:f9iiig2zzo@mongodb.selfmade.ninja:27017/?authSource=users";

// async function connectToMongoDB() {
//   try {
//     const client = await MongoClient.connect(url, { useNewUrlParser: true });

//     // Specify the database and collection you want to work with
//     const dbName = "kavin_contacts";
//     const collectionName = "contacts";
//     const db = client.db(dbName);
//     const collection = db.collection(collectionName);

//     // Find all documents in the "employees" collection
//     const documents = await collection.find({}).toArray();

//     // Log the retrieved documents
//     console.log("Retrieved documents:");
//     console.log(documents);

//     // Close the connection
//     client.close();
//   } catch (err) {
//     console.error("Error connecting to MongoDB:", err);
//   }
// }

// connectToMongoDB();
const mongoose = require("mongoose");

const url = "mongodb://kavin:f9iiig2zzo@mongodb.selfmade.ninja:27017/kavin_contacts?authSource=users";

async function connectToMongoDB() {
  try {
    await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
    console.log("Connected to MongoDB!");

    // Define a Mongoose model for the "contacts" collection
    const Contact = mongoose.model("Contact", {
      name: String,
      email: String,
      // Define other fields as needed
    });

    // Fetch all documents from the "contacts" collection
    const documents = await Contact.find();

    // Log the retrieved documents
    console.log("Retrieved documents:");
    console.log(documents);

    // Close the connection
    mongoose.connection.close();
  } catch (err) {
    console.error("Error connecting to MongoDB:", err);
  }
}

connectToMongoDB();
