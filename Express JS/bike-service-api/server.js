const express = require("express");
const connectDb = require("./config/dbConnection");
const errorHandler = require("./middleware/errorHandler");
const dotenv = require("dotenv").config();

connectDb();
const app = express();

const port = process.env.PORT || 4000;
app.use(express.json());

// Use routes
app.use('/api/', require('./routes/index'));
app.use('/api/users',  require('./routes/users'));
app.use('/api/bookings', require('./routes/bookings'));
app.use('/api/services', require('./routes/services'));

app.get("/api/test", (req, res) => {
  res.status(200).json(req.headers);
});

app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server running on Port ${port}`);
});
