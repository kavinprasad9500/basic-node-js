const http = require('http')
const date = require('./date')

http.createServer(function(req, res){
   res.writeHead(200, {'Content-type': 'text/html'});
   res.write("Current Date : " + date.myDate());
   res.end()

}).listen(4444);