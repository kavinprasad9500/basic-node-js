const asyncHandler = require("express-async-handler");
const dotenv = require("dotenv").config();

const jwt = require("jsonwebtoken");

const validateToken = asyncHandler(async (req, res, next) => {
  let token;
  let authHeader = req.headers.Authorization || req.headers.authorization;
  if (authHeader && authHeader.startsWith("Bearer")) {
    token = authHeader.split(" ")[1];
    if (!token) {
      res.status(401);
      throw new Error("User is not Authorized or Token Missing");
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, { maxAge: '15m' }, (err, decoded) => {
      if (err) {
        res.status(401);
        if (err.name === 'TokenExpiredError') {
          throw new Error("Token has expired");
        } else {
          throw new Error("User is not Authorized");
        }
      }
      req.user = decoded.user;
      next();
    });
  } else {
    res.status(401);
    throw new Error("User is not Authorized or Token Missing");
  }
});

module.exports = validateToken;
