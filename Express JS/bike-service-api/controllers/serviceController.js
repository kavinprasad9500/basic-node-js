const asyncHandler = require("express-async-handler");
const Service = require("../models/ServiceModel");

// @desc    Get all services
// @route   GET /api/services
// @access  Private (requires a valid token)
const getAllServices = asyncHandler(async (req, res) => {
  const services = await Service.find();
  res.status(200).json(services);
});

// @desc    Get a service by ID
// @route   GET /api/services/:id
// @access  Private (requires a valid token)
const getService = asyncHandler(async (req, res) => {
  const serviceId = req.params.id;

  const service = await Service.findById(serviceId);

  if (!service) {
    res.status(404);
    throw new Error("Service not found");
  }

  res.status(200).json(service);
});

// @desc    Create a new service
// @route   POST /api/services
// @access  Private (requires a valid token)
const createService = asyncHandler(async (req, res) => {
  const { name, description, price } = req.body;

  if (!name || !description || !price) {
    res.status(400);
    throw new Error("All fields are mandatory");
  }

  const newService = new Service({
    name,
    description,
    price,
  });

  await newService.save();

  res.status(201).json({ message: "Service created successfully", service: newService });
});

// @desc    Update a service by ID
// @route   PUT /api/services/:id
// @access  Private (requires a valid token)
const updateService = asyncHandler(async (req, res) => {
  const serviceId = req.params.id;
  const { name, description, price } = req.body;

  try {
    const service = await Service.findById(serviceId);

    if (!service) {
      res.status(404);
      throw new Error("Service not found");
    }

    service.name = name;
    service.description = description;
    service.price = price;

    await service.save();

    res.status(200).json({ message: "Service updated successfully", service });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Failed to update service" });
  }
});

// @desc    Delete a service by ID
// @route   DELETE /api/services/:id
// @access  Private (requires a valid token)
const deleteService = asyncHandler(async (req, res) => {
  const serviceId = req.params.id;

  try {
    const service = await Service.findById(serviceId);

    if (!service) {
      res.status(404);
      throw new Error("Service not found");
    }

    const deletedService = await Service.findByIdAndRemove(serviceId);

    if (!deletedService) {
      res.status(500).json({ error: "Failed to delete service" });
      return;
    }

    res.status(200).json({ message: "Service deleted successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Failed to delete service" });
  }
});

module.exports = {
  getAllServices,
  getService,
  createService,
  updateService,
  deleteService,
};
