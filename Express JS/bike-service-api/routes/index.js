const express = require('express');
const router = express.Router();

// Define your index route
router.get('/', (req, res) => {
  res.send('Welcome to the Bike Service Application!');
});

module.exports = router;
