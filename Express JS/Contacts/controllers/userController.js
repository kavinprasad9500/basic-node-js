const asyncHandler = require("express-async-handler");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/userModel");

//@desc Regiter a user
//@route POST /api/user/register
//@access public
const registerUser = asyncHandler(async (req, res) => {
  const { username, email, password } = req.body;
  if (!username || !email || !password) {
    res.status(400);
    throw new Error("All Fileds are mandatory");
  }
  const userAvailable = await User.findOne({ email });
  if (userAvailable) {
    res.status(400);
    throw new Error("User already register");
  }
  // HashPassword
  const hashedPass = await bcrypt.hash(password, 10);
  // console.log(username, email, password);
  // console.log(hashedPass);
  const user = await User.create({
    username,
    email,
    password: hashedPass,
  });
  // console.log(`User Created ${user.id} --- ${user.email}`);
  if (user) {
    res.status(201).json({ _id: user.id, email: user.email });
  } else {
    res.status(400);
    throw new Error("user data is not Valid");
  }
});

//@desc Login  user
//@route POST /api/user/login
//@access public
const loginUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(400);
    throw new Error("All Fields are mandatory");
  }
  const user = await User.findOne({ email });
  // compare passowrd with hash
  if (user && bcrypt.compare(password, user.password)) {
    // Provide Access Token
    const accessToken = jwt.sign(
      {
        user: {
          username: user.username,
          email: user.email,
          id: user.id,
        },
      },
      process.env.ACCESS_TOKEN_SECERT,
      { expiresIn: "15m" }
    );
    res.status(200).json({ accessToken });
  }
  else{
    res.status(401);
    throw new Error("Email or Password is not Valid");
  }
  res.json({ message: "Login the User" });
});

//@desc Current  user
//@route GET /api/user/current
//@access private
const currentUser = asyncHandler(async (req, res) => {
  // res.json({ message: "Current User Info" });
  res.json(req.user);
});

module.exports = { registerUser, loginUser, currentUser };
