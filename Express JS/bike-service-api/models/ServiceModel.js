// models/service.js
const mongoose = require('mongoose');

// Define the service schema
const serviceSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
});

// Create the Service model
module.exports = mongoose.model('Service', serviceSchema);
