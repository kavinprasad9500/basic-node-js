const express = require("express");
const router = express.Router();
const {
  getAllBookings,
  getBookingDetails,
  createBooking,
  updateBooking,
  deleteBooking,
  getUserBookings
} = require("../controllers/bookingController");
const validateToken = require("../middleware/validateTokenHandler");

router.use(validateToken);

// Get all bookings for a user
// Create a new booking
router.route("/").get(getAllBookings).post(createBooking);

// Get Specific User Bookings
router.route("/userbooking/:id").get(getUserBookings);

// Get Booking Details && Update Booking && Delete Booking
router.route("/:id").get(getBookingDetails).put(updateBooking).delete(deleteBooking);

module.exports = router;
