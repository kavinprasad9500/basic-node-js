// exports.constants = {
//     // Informational
//     CONTINUE: 100,                  // Continue
//     SWITCHING_PROTOCOLS: 101,       // Switching Protocols
//     PROCESSING: 102,               // Processing

//     // Successful
//     OK: 200,                        // OK
//     CREATED: 201,                   // Created
//     ACCEPTED: 202,                  // Accepted
//     NO_CONTENT: 204,               // No Content
//     RESET_CONTENT: 205,            // Reset Content
//     PARTIAL_CONTENT: 206,         // Partial Content

//     // Redirection
//     MULTIPLE_CHOICES: 300,         // Multiple Choices
//     MOVED_PERMANENTLY: 301,        // Moved Permanently
//     FOUND: 302,                    // Found
//     SEE_OTHER: 303,                // See Other
//     NOT_MODIFIED: 304,             // Not Modified
//     USE_PROXY: 305,                // Use Proxy
//     TEMPORARY_REDIRECT: 307,       // Temporary Redirect
//     PERMANENT_REDIRECT: 308,       // Permanent Redirect

//     // Client Errors
//     BAD_REQUEST: 400,              // Bad Request
//     UNAUTHORIZED: 401,             // Unauthorized
//     FORBIDDEN: 403,                // Forbidden
//     NOT_FOUND: 404,                // Not Found
//     METHOD_NOT_ALLOWED: 405,       // Method Not Allowed
//     CONFLICT: 409,                 // Conflict
//     GONE: 410,                     // Gone
//     LENGTH_REQUIRED: 411,          // Length Required
//     PRECONDITION_FAILED: 412,      // Precondition Failed
//     PAYLOAD_TOO_LARGE: 413,        // Payload Too Large
//     URI_TOO_LONG: 414,             // URI Too Long
//     UNSUPPORTED_MEDIA_TYPE: 415,   // Unsupported Media Type
//     RANGE_NOT_SATISFIABLE: 416,    // Range Not Satisfiable
//     EXPECTATION_FAILED: 417,       // Expectation Failed
//     UNPROCESSABLE_ENTITY: 422,     // Unprocessable Entity
//     UPGRADE_REQUIRED: 426,         // Upgrade Required
//     PRECONDITION_REQUIRED: 428,    // Precondition Required
//     TOO_MANY_REQUESTS: 429,        // Too Many Requests
//     REQUEST_HEADER_FIELDS_TOO_LARGE: 431,  // Request Header Fields Too Large

//     // Server Errors
//     INTERNAL_SERVER_ERROR: 500,     // Internal Server Error
//     NOT_IMPLEMENTED: 501,           // Not Implemented
//     BAD_GATEWAY: 502,               // Bad Gateway
//     SERVICE_UNAVAILABLE: 503,       // Service Unavailable
//     GATEWAY_TIMEOUT: 504,           // Gateway Timeout
//     HTTP_VERSION_NOT_SUPPORTED: 505,  // HTTP Version Not Supported
//     NETWORK_AUTHENTICATION_REQUIRED: 511  // Network Authentication Required
// };


exports.constants = {
    VALIDATION_ERROR: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    SERVER_ERROR: 500,
  };