const mongoose = require("mongoose");

const connectDb = async () => {
  try {
    const connect = await mongoose.connect(process.env.CONNECTION_STRING, {
      useNewUrlParser: true
    });
    // console.log("Database Log : " , connect.connection.host);
    console.log(
      "Database connected","=>",
      connect.connection.host,"=>", 
      connect.connection.name
    );
    console.log("Connected to MongoDB");
  } catch (err) {
    console.error("MongoDB connection error:", err);
    process.exit(1);
  }
};

module.exports = connectDb;
