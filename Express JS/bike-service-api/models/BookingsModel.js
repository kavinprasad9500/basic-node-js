const mongoose = require("mongoose");

const bookingSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User", // Reference to the User model
    required: true,
  },
  bikeModel: {
    type: String,
    required: true,
  },
  service: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Service", // Reference to the Service model
      required: true,
    },
  ],
  bookingDate: Date,
  status: {
    type: String,
    enum: ["Pending", "Ready For Delivery", "Completed"],
    default: "Pending",
  },
});

const Booking = mongoose.model("Booking", bookingSchema);

module.exports = Booking;
