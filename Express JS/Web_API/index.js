const express = require("express");
const app = express();
const {logger} = require('./middleWare/logEvents')
const path = require("path");
const cors = require('cors');
const errorHandler = require('./middleWare/errorHandler');
const corsOptions = require("./config/corsOprions");
const port = process.env.PORT || 4444;

app.use(logger);

// Cross origin Resource Sharing
app.use(cors(corsOptions));


app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use('/', express.static(path.join(__dirname, './public')));

app.use('/', require('./routes/root'));
app.use('/api/employees', require('./routes/api/employees'));

// app.get("/hello(.html)?", (req, res , next) => {
//     console.log("Trying to Load Hello Page")
//     next()
// },(req, res) =>{
//     res.send("Hello This is Kavin");
// });

// app.get("/*", (req, res) => {
//     res.status(404).sendFile(path.join(__dirname, "app", "404.html"));
// });

app.all('*' ,(req, res) =>{
  res.status(404);
  if(req.accepts('html'))
    res.sendFile(path.join(__dirname, "app", "404.html"));
  else if(req.accepts('json'))
    res.json({"error": "404 Not Found"});
  else 
    res.type('txt').send("404 Not Found");
})


app.use(errorHandler)

app.listen(port, () => {
  console.log(`Local Host listening on port ${port}`);
});
