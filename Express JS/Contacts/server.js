const express = require("express");
const connectDb = require("./config/dbConnection");
const errorHandler = require("./middleware/errorHandler");
const dotenc = require("dotenv").config();

connectDb();
const app = express();

const port = process.env.PORT || 4000;
app.use(express.json());

app.use("/api/contacts", require("./routes/contactRoutes"));
app.use("/api/user", require("./routes/userRoutes"));

app.get("/api/test", (req, res) => {
  res.status(200).json(req.headers);
});

app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server running on Port ${port}`);
});
