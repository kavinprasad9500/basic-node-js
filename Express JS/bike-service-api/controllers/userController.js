const asyncHandler = require("express-async-handler");
// const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/UserModel");

//@desc Register a user
//@route POST /api/user/register
//@access public
const registerUser = asyncHandler(async (req, res) => {
  const { username, email, password, mobileNumber } = req.body;
  if (!username || !email || !password || !mobileNumber) {
    res.status(400);
    throw new Error("All fields are mandatory");
  }
  const userAvailable = await User.findOne({ email });
  if (userAvailable) {
    res.status(400);
    throw new Error("User already registered");
  }
  // HashPassword
  const hashedPass = password;
  // const hashedPass = await bcrypt.hash(password, 10);
  const user = await User.create({
    username,
    email,
    password: hashedPass,
    mobileNumber,
  });
  if (user) {
    res.status(201).json({ _id: user.id, email: user.email, mobileNumber: user.mobileNumber, password: user.password });
  } else {
    res.status(400);
    throw new Error("User data is not valid");
  }
});

//@desc Login user
//@route POST /api/user/login
//@access public
const loginUser = asyncHandler(async (req, res) => {
  const { emailOrMobile, password } = req.body;
  if (!emailOrMobile || !password) {
    res.status(400);
    throw new Error("All fields are mandatory");
  }

  // Check if the input is an email or a mobile number
  const isEmail = /\S+@\S+\.\S+/.test(emailOrMobile);
  const isMobileNumber = /^\d{10}$/.test(emailOrMobile);
  
  let user;

  if (isEmail) {
    user = await User.findOne({ email: emailOrMobile });
  } else if (isMobileNumber) {
    user = await User.findOne({ mobileNumber: emailOrMobile });
  }
  if(!user) {
    res.status(401);
    throw new Error("Please Create an Account");
  }
  // console.log(password + "  " +user.password);/
  // Compare password with hash
  // if (user && bcrypt.compareSync(password, user.password)) {
    if (user && password==user.password) {
      // Provide Access Token
      // console.log(process.env.ACCESS_TOKEN_SECRET);
      const expiresIn = "24h"; // Change this to your desired expiration time
  
      const accessToken = jwt.sign(
        {
          user: {
            username: user.username,
          email: user.email,
          id: user.id,
        },
      },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn }
      );
      // console.log(accessToken);
    res.status(200).json({ accessToken });
  } else {
    res.status(401);
    throw new Error("Email or Mobile Number and Password is not valid");
  }
});


//@desc Current user
//@route GET /api/user/current
//@access private
const currentUser = asyncHandler(async (req, res) => {
  res.json(req.user);
});

module.exports = { registerUser, loginUser, currentUser };
