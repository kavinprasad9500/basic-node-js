const { format } = require('date-fns');
const {v4: uuid} = require('uuid');

const fs = require('fs')
const fsPromises = require('fs').promises
const path = require('path')

const logEvents = async (message, fileName) => {
    const dateTime = `${format(new Date(), 'ddMMyyy\tHH:mm:ss')}`;
    const logItem = `${dateTime}\t${uuid()}\t${message}`;
    // console.log(logItem);
    try{
        if(!fs.existsSync(path.join(__dirname, '..', 'logs')))
            await fsPromises.mkdir(path.join(__dirname, '..', 'logs'))
        await fsPromises.appendFile(path.join(__dirname, '..', 'logs', fileName), logItem);
    }
    catch (err){
        console.error(err);
    }
}

const logger = async (req,res, next) => {
    await logEvents(`${req.method}\t${req.headers.origin}\t${req.path}\n`, 'reqLog.txt')
    // console.log(`${req.method} ${req.path}`);
    next()
  }
  
module.exports = {logEvents, logger};