const asyncHandler = require("express-async-handler");
const Booking = require("../models/BookingsModel");
const User = require("../models/UserModel");
const sendEmail = require('../MailSender');

// ############ Admin AREA ####################
//@desc Get all Bookings
//@route GET /api/Bookings
//@access private
const getAllBookings = asyncHandler(async (req, res) => {
  const bookings = await Booking.find().populate("user").populate("service");
  res.status(200).json(bookings);
});

//@desc Update a Booking
//@route PUT /api/Booking/:id
//@access private
const updateBooking = asyncHandler(async (req, res) => {
  const bookingId = req.params.id;
  const { service, status } = req.body;

  if (!bookingId || !status) {
    res.status(400);
    throw new Error("All fields are mandatory");
  }
  // Find the booking to be updated by its ID
  const booking = await Booking.findById(bookingId);

  if (!booking) {
    res.status(404);
    throw new Error("Booking not found");
  }

  // Update the booking fields with the new data
  booking.service = service;
  booking.status = status;

  // Save the updated booking to the database
  await booking.save();

  const userDetail = await User.findOne({_id: booking.user});
  // console.log(userDetail.email);
  await sendEmail(userDetail,`Bruno Bike Service ${status}`, booking);
  res.status(200).json({ message: "Booking updated successfully", booking });
});

//@desc Delete a Booking
//@route PUT /api/Booking/:id
//@access private
const deleteBooking = asyncHandler(async (req, res) => {
  if (!req.params.id) {
    res.status(400);
    throw new Error("All fields are mandatory");
  }
  const bookings = await Booking.findById(req.params.id);
  //   console.log(Booking);

  //   console.log(req.params.id);
  if (!bookings) {
    res.status(404);
    throw new Error("Booking not found");
  }
  const deletedBooking = await Booking.findByIdAndRemove(req.params.id);

  if (!deletedBooking) {
    res.status(500).json({ error: "Failed to delete Booking" });
    return;
  }

  res.status(200).json({ Title: "Deleted SuccessFully" });
  // res.status(200).json(deletedBooking);
});

//@desc Create a Booking
//@route POST /api/Booking/:id
//@access private
const createBooking = asyncHandler(async (req, res) => {
  // Extract the booking data from the request body
  const {bikeModel, user, service } = req.body; // This should be the ObjectId
  if (!bikeModel || !user || !service) {
    res.status(400);
    throw new Error("All fields are mandatory");
  }
  const currentDate = new Date().toISOString().split("T")[0];

  // Create a new booking instance
  const newBooking = new Booking({
    user, // Assign the user ObjectId
    bikeModel, // Assign the Bike Name
    service, // Assign the service ObjectId
    bookingDate: currentDate,
  });

  // Save the new booking to the database
  await newBooking.save();
  // console.log(newBooking.user);
  const userDetail = await User.findOne({_id: newBooking.user});
  if (!userDetail){
    res.status(404);
    throw new Error("User not Found Please Login");
  }
  // console.log(userDetail.email);
  await sendEmail(userDetail,`Bruno Bike Service Appointment Confirmation`, newBooking);

  res
    .status(201)
    .json({ message: "Booking created successfully", booking: newBooking });
});

//@desc Get a Booking
//@route GET /api/Booking/:id
//@access private
const getBookingDetails = asyncHandler(async (req, res) => {
  if (!req.params.id) {
    res.status(400);
    throw new Error("All fields are mandatory");
  }
  const booking = await Booking.findById(req.params.id);
  if (!booking) {
    res.status(404);
    throw new Error("Booking not found");
  }
  res.status(200).json(booking);
});

//@desc Get User a Booking
//@route GET /api/Booking/:id
//@access private
const getUserBookings = asyncHandler(async (req, res) => {
  const userId = req.params.id; // Get the user's ID from the request params

  if (!userId) {
    res.status(400);
    throw new Error("User ID is mandatory");
  }
    // Find all bookings for the user with the specified ID
    const bookings = await Booking.find({ user: userId });
    if (!bookings) {
      res.status(404);
      throw new Error("Bookings not found for this user");
    }
    res.status(200).json(bookings);
});


module.exports = {
  getAllBookings,
  getBookingDetails,
  getUserBookings,
  createBooking,
  updateBooking,
  deleteBooking,
};
