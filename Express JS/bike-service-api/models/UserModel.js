const mongoose = require("mongoose");

const userSchema = mongoose.Schema(
  {
    username: {
      type: String,
      required: [true, "Please add the username"],
    },
    email: {
      type: String,
      required: [true, "Please add the user email address"],
      unique: [true, "Email address already taken"],
      validate: {
        validator: function (v) {
          // Check if the email follows a valid format
          return /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(v);
        },
        message: "Please enter a valid email address",
      },
    },
    password: {
      type: String,
      required: [true, "Please add the user password"],
      validate: {
        validator: function (v) {
          // Check if the password has a minimum length of 8 characters
          return v.length >= 8;
        },
        message: "Password must be at least 8 characters long",
      },
    },
    mobileNumber: {
      type: String,
      validate: {
        validator: function (v) {
          // Check if the mobile number is exactly 10 digits
          return /^\d{10}$/.test(v);
        },
        message: "Mobile number must be exactly 10 digits",
      },
      required: [true, "Please add the user mobile number"],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("User", userSchema);
