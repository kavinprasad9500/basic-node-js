const express = require('express');
const router = express.Router();
const {
  getAllServices,
  getService,
  createService,
  updateService,
  deleteService,
} = require("../controllers/serviceController");
const validateToken = require("../middleware/validateTokenHandler");

router.use(validateToken);

// Example route to list all services
// Create a new Services
router.route("/").get(getAllServices).post(createService);

// // Get all Servicess for a user
router.route("/:id").get(getService).put(updateService).delete(deleteService);

module.exports = router;
 